<?php

namespace Bytesize\Transcoder;


use Exception;
use InvalidArgumentException;

define("BASE_API", "http://localhost:8081/");

class FileDoesNotExist extends \RuntimeException {}
class FileAlreadyFullyUploaded extends \RuntimeException {}

/**
 * Used to upload new files to the transcoder cloud storage.
 * Class Uploader
 * @package Bytesize\Transcoder
 */
class Uploader
{
    private $API_UPLOAD_ENDPOINT = "v1/upload/chunks";
    private $API_FILE_STATUS_ENDPOINT = "v1/browse/search";
    private $api_key;

    function __construct($apiKey)
    {
        $this->api_key = $apiKey;
    }

    /**
     * Used to upload a file in small chunks rather than all at once.
     * If transfer is interrupted then once you try to upload the same
     * file it will resume starting from the last chunk that was uploaded.
     * @param string $filePath
     * @param string $dir
     * @param int $chunk_size
     * @return void
     */
    function upload($filePath, $dir, $chunk_size = 5242880)
    {
        $fileInfo = null;
        try {
            $fileInfo = $this->get_file_status(basename($filePath), $dir);
            if ($fileInfo->isAvailable) {
                throw new FileAlreadyFullyUploaded("this file is fully uploaded and it cannot be modified");
            }
        } catch (Exception $e) {}
        $fileSize = filesize($filePath);
        $start = 0;
        $end = $chunk_size;

        while ($end <= $fileSize) {
            if ($fileInfo) {
                $isUsed = $this->checkPart($fileInfo, $start, $end);
                while ($isUsed !== null && $end <= $fileSize) {
                    if ($start < $isUsed->rangeStart && $end > ($start + $isUsed->size)) {
                        $end = $isUsed->rangeStart;
                    } else {
                        $start += $isUsed->size;
                        $end = $start + $chunk_size;
                        if ($end > $fileSize) {
                            $end = $fileSize;
                        }
                    }
                    $isUsed = $this->checkPart($fileInfo, $start, $end);
                }
            }
            $this->upload_part($filePath, $start, $end - $start);
            $start = $end;
            if ($start >= $fileSize) {
                break;
            }
            $end = $start + $chunk_size;
            if ($end >= $fileSize) {
                $end = $fileSize;
            }
        }
    }

    /**
     * Returns details about a chosen file
     * @param string $fileName
     * @param string $dir
     * @return array
     * @throws Exception
     */
    function get_file_status($fileName, $dir)
    {
        $options = array(
            'http' => array(
                'ignore_errors' => true
            )
        );
        $context = stream_context_create($options);
        $response = @file_get_contents(BASE_API . $this->API_FILE_STATUS_ENDPOINT . '?apiKey=' . $this->api_key . '&fileName=' . $fileName . '&dir=' . $dir, false, $context);
        $response = json_decode($response);

        if ($http_response_header[0] !== "HTTP/1.0 200 OK") {
            throw new Exception("get_file_status failed: " . $response->data[0]);
        }

        usort($response->data[0]->parts, function ($a, $b) {
            return $a->rangeStart > $b->rangeStart;
        });

        return $response->data[0];
    }

    private function checkPart($fileInfo, $startRange, $endRange)
    {
        $endRange -= 1;
        foreach ($fileInfo->parts as $part) {
            $partRangeEnd = $part->rangeStart + $part->size - 1;
            if ($startRange >= $part->rangeStart && $startRange <= $partRangeEnd) {
                print 1;
                return $part;
            } else if ($endRange >= $part->rangeStart && $endRange <= $partRangeEnd) {
                print 2;
                return $part;
            } else if ($part->rangeStart <= $startRange && $endRange <= $part->rangeStart) {
                print 3;
                return $part;
            } else if ($startRange >= $part->rangeStart && $endRange <= $partRangeEnd) {
                print 4;
                return $part;
            } else if ($startRange <= $part->rangeStart && $endRange >= $partRangeEnd) {
                print 5;
                return $part;
            }
        }
        return null;
    }

    private function generate_hash($filePath) {
        $length = 4096;
        $file_size = filesize($filePath);
        if($file_size < $length){
            $length = $file_size;
        }
        $fp = fopen($filePath, 'r');
        $data = fread($fp, $length);
        return md5($data);
    }

    /**
     * Allows to upload a chunk of a file.
     * This function will not check whether this file is already fully uploaded,
     * if it's already uploaded server will return an error.
     * @param string $filePath
     * @param int $offset
     * @param int $length
     * @return void
     */
    function upload_part($filePath, $offset = 0, $length = 0)
    {
        if(!file_exists($filePath)){
            throw new FileDoesNotExist("the given file path is invalid or there is no such file");
        }
        if ($length < 1) {
            throw new InvalidArgumentException("length cannot be smaller than 1");
        }
        $request = curl_init(BASE_API . $this->API_UPLOAD_ENDPOINT . '?apiKey=' . $this->api_key);
        $fp = fopen($filePath, 'r');
        fseek($fp, $offset);
        $data = fread($fp, $length);
        $fields = array(
            'fileName' => basename($filePath),
            'hash' => $this->generate_hash($filePath),
            'fileSize' => filesize($filePath),
            'dir' => "/",
            'start' => $offset,
        );
        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;
        $post_data = $this->build_data($boundary, $fields, $data, $fields['fileName']);
        curl_setopt_array($request, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)
            ),
        ));

        $result = curl_exec($request);
        if ($result === false) {
            echo "Curl failed with error: ", curl_error($request);
        } else {
            echo $result . "\n";
        }

        curl_close($request);
    }

    private function build_data($boundary, $fields, $file, $fileName)
    {
        $data = '';
        $eol = "\r\n";

        $delimiter = '-------------' . $boundary;

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"" . $eol . $eol
                . $content . $eol;
        }

        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="file"; filename="' . $fileName . '.part"' . $eol
            . 'Content-Transfer-Encoding: binary' . $eol;

        $data .= $eol;
        $data .= $file . $eol;

        $data .= "--" . $delimiter . "--" . $eol;


        return $data;
    }
}


